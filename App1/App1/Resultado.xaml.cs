﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Resultado : ContentPage
	{
		public Resultado (string nome, Double val1, Double val2)
		{
			InitializeComponent ();
            fulano.Text = nome + " O resultado foi: ";

            soma.Text = "A soma é : " + (val1+val2);
            subtracao.Text = "A subtração é : " + (val1 - val2);
            if (val2 > 0)
            {
                divisao.Text = "A divisão é : " + (val1 / val2);
            }
            multiplicacao.Text = "A multiplicação é : " + (val1 * val2);
        }
	}
}